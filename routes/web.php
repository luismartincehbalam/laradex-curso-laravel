<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('pokemons','PokemonController');
Route::get('name/{name}', function ($name){

    return 'Hola soy:' . $name;
});

//Acceder a un controlador
Route::get('prueba','Controller@prueba');

//Acceder a todas las rutas de un controlador del mismo tipo
Route::resource('trainers', 'TrainerController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
