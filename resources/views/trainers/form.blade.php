<div class="container">
<div class="form-group">
    <form class="form-group" method="POST" action="/trainers" enctype="multipart/form-data">
        @csrf <!--Funcion de seguridad-->
        <label for="">Nombre</label>
        <input type="text"  name="name" class="form-control" >
        <label for="">Slug</label>
        <input type="text" name="slug"  class="form-control">
        <label for="">Avatar</label>
        <input type="file" name="avatar">
        <br>
        <label for="">Descripcion</label>
        <input type="text" name="description"  class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Guardar</button>
    </form>
</div>