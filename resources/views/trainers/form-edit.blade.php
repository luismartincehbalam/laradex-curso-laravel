<div class="form-group">
    <form class="form-group" method="POST" action="/trainers/{{$trainer->slug}}" enctype="multipart/form-data">        
        @method('PUT')
        @csrf <!--Funcion de seguridad-->
        <label for="">Nombre</label>
        <input type="text"  name="name" class="form-control" value="{{$trainer->name}}">
        <label for="">Slug</label>
        <input type="text" name="slug"  value="{{$trainer->slug}}"  class="form-control">
        <label for="">Avatar</label> 
        <input type="text" name="avatar" value="{{$trainer->avatar}}" disabled>
        <input type="file" name="avatar" >
        
        <br>
        <label for="">Descripcion</label>
        <input type="text" name="description" value="{{$trainer->description}}" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Actualizar</button>
    </form>